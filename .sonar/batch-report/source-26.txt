/**
 * TransactionItemHistory.js
 *
 * @description :: Model representation of a TransactionItemHistory from MER view.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    transactionItemHistoryId: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'transaction_item_history_id'
    },
    amount: {
      type: Sequelize.NUMERIC,
      allowNull: false,
      defaultValue: 0
    },
    installments: {
      type: Sequelize.INTEGER
    },
    paymentNumber: {
      type: Sequelize.STRING,
      field: 'payment_number'
    },
    paymentMethod: {
      type: Sequelize.STRING,
      field: 'payment_method',
      allowNull: false
    },
    status: {
      type: Sequelize.STRING,
      allowNull: false 
    }
  },
  associations: function() {
    TransactionItemHistory.belongsTo(OppUser, { foreignKey : { name: 'updatedBy', field: 'updated_by' } });
    TransactionItemHistory.belongsTo(CreditCard, { foreignKey : { name: 'creditCardId', field: 'credit_card_id' } });
    TransactionItemHistory.belongsTo(OppTransaction, { foreignKey : { name: 'transactionId', field: 'transaction_id' } });
    TransactionItemHistory.belongsTo(TransactionItem, { foreignKey : { name: 'transactionItemId', field: 'transaction_item_id' } });
  },
  options: {
    tableName: 'transaction_item_history',
    classMethods: {},
    instanceMethods: {
      toJSON: function () {
        var values = Object.assign({}, this.get());
            values.createdAt = values.created_at;
            values.updatedAt = values.updated_at;
        delete values.created_at;
        delete values.updated_at; 
        return values;
      }
    },
    hooks: {},
    underscored: true
  }
};

