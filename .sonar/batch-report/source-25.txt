/**
 * TransactionItem.js
 *
 * @description :: Model representation of a TransactionItem from MER view.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    transactionItemId: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'transaction_item_id'
    },
    amount: {
      type: Sequelize.NUMERIC,
      allowNull: false,
      defaultValue: 0
    },
    installments: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: 1,
      validate: {
        is: {
          args: /^[1-9]\d*$/i,
          msg: new InputErrorService.invalidInstallments().message
        }
      }
    },
    paymentNumber: {
      type: Sequelize.STRING,
      field: 'payment_number'
    },
    paymentMethod: {
      type: Sequelize.ENUM,
      values: ['creditcard', 'funds', 'loan'],
      field: 'payment_method',
      allowNull: false
    },
    status: {
      type: Sequelize.ENUM,
      values: ['pending', 'done', 'rejected', 'canceled'],
      allowNull: false 
    }
  },
  associations: function() {
    TransactionItem.belongsTo(OppUser, { foreignKey : { name: 'updatedBy', field: 'updated_by', allowNull: false } });
    TransactionItem.belongsTo(CreditCard, { foreignKey : { name: 'creditCardId', field: 'credit_card_id' } });
  },
  options: {
    tableName: 'transaction_item',
    classMethods: {},
    instanceMethods: {
      toJSON: function () {
        var values = Object.assign({}, this.get());
            values.createdAt = values.created_at;
            values.updatedAt = values.updated_at;
        delete values.created_at;
        delete values.updated_at; 
        return values;
      }
    },
    hooks: {},
    underscored: true
  }
};

