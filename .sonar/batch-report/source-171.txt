<!doctype html>
<html lang="en">
<head>
    <title>Code coverage report for api/controllers/OppDepositController.js</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../../prettify.css" />
    <link rel="stylesheet" href="../../base.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type='text/css'>
        .coverage-summary .sorter {
            background-image: url(../../sort-arrow-sprite.png);
        }
    </style>
</head>
<body>
<div class='wrapper'>
  <div class='pad1'>
    <h1>
      <a href="../../index.html">all files</a> / <a href="index.html">api/controllers/</a> OppDepositController.js
    </h1>
    <div class='clearfix'>
      <div class='fl pad1y space-right2'>
        <span class="strong">100% </span>
        <span class="quiet">Statements</span>
        <span class='fraction'>45/45</span>
      </div>
      <div class='fl pad1y space-right2'>
        <span class="strong">100% </span>
        <span class="quiet">Branches</span>
        <span class='fraction'>26/26</span>
      </div>
      <div class='fl pad1y space-right2'>
        <span class="strong">100% </span>
        <span class="quiet">Functions</span>
        <span class='fraction'>10/10</span>
      </div>
      <div class='fl pad1y space-right2'>
        <span class="strong">100% </span>
        <span class="quiet">Lines</span>
        <span class='fraction'>36/36</span>
      </div>
    </div>
  </div>
  <div class='status-line high'></div>
<pre><table class="coverage">
<tr><td class="line-count quiet">1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
90
91
92
93
94
95
96
97
98
99
100
101
102
103
104
105
106
107
108
109
110
111
112</td><td class="line-coverage quiet"><span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">1×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">9×</span>
<span class="cline-any cline-yes">9×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">8×</span>
<span class="cline-any cline-yes">8×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">6×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-yes">4×</span>
<span class="cline-any cline-yes">3×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">2×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">2×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">2×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">2×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">2×</span>
<span class="cline-any cline-yes">2×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">3×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">6×</span>
<span class="cline-any cline-yes">6×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">4×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">4×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">4×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">5×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">3×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-yes">2×</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span>
<span class="cline-any cline-neutral">&nbsp;</span></td><td class="text"><pre class="prettyprint lang-js">/**
 * DepositController
 *
 * @description :: Server-side logic for managing deposits
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
&nbsp;
module.exports = {
    deposit: function (req, res) {
        var currentUser = req.session.me || null;
        if (!currentUser) return res.json(401, { error: new DatabaseErrorService.unauthorized() });
&nbsp;
        // Validation Data
        var validation = ValidationService.deposit(req.body);
        if (validation.statusCode) return res.json(validation.statusCode, {error: validation});
&nbsp;
        // Check if value of transaction is posite
        if (+req.body.amount &lt; 0) return res.json(400, { error: new InputErrorService.negativeAmount() });
        // Creating a new transaciton session
        OppTransaction.sequelize.transaction({ autocommit: false }, function (t) {
            // Finding refered user and OPP user
            return OppUser.findAll({
                where: {
                    $or: [{ userId: +req.body.userId }, { blockchainHash: 'OPP' }]
                },
                transaction: t
            }).then(function (users) {
                // Getting from User
                var user = _.findWhere(users, { userId: +req.body.userId });
                // getting Opp user
                var oppUser = _.findWhere(users, { blockchainHash: 'OPP' });
                if (!user) throw new DatabaseErrorService.notFound(sails.__('_user'));
                if (!oppUser) throw new DatabaseErrorService.notFound(sails.__('_user'));
                if (user == oppUser) throw new InputErrorService.sameUserTransaction();
                // Creating the transaction
                return OppTransaction.create({
                    totalAmount: +req.body.amount,
                    status: 'pending',
                    type: 'deposit',
                    toUserId: user.userId,
                    fromUserId: oppUser.userId
                }, { transaction: t })
                .then(function (tx) {
                    // Building Transaction item obj
                    var txItemData = {
                        amount: +req.body.amount,
                        paymentMethod: 'funds',
                        status: 'pending',
                        updatedBy: oppUser.userId
                    }
                    // Creating transaction Item
                    return TransactionItem.create(txItemData, { transaction: t })
                    .then(function (txItem) {
                        // Association with transaction item
                        return tx.addTransactionItem(txItem, { transaction: t })
                        .then(function (updated) {
                            // Addicting transaction Id to response object 
                            user.dataValues.transactionId = updated.transactionId;
                            return res.json(200, user);
                        })
                    })
                })
            })
        })
        .catch(function (err) {
            DatabaseErrorService.log(err, res)
        })
    },
    list: function (req, res) {
        var currentUser = req.session.me || null;
        if (!currentUser) return res.json(401, { error: new DatabaseErrorService.unauthorized() });
        // Querying object
        var q = {
            where: {
                type:'deposit',
                $and: {
                    status: 'pending'
                }
            }
        }
        var params = req.query;
        // Filtering by number of days
        if (params.days) {
            // Current date
            var date = new Date();
            // Subtract days
            date.setDate(date.getDate() - +req.param('days'))
            // AND query
            q.where.$and.updated_at = {
                $gte: date
            }
        }
        
        var limit = params.limit;
        var page = params.page;
        // Offset to pagination
        var offset = (+page * limit);
        // Filtering page limit
        if(limit) q.limit = +limit;
        if(page &amp;&amp; limit) q.offset = offset;
        // Get and return all transaction
        return OppTransaction.findAll(q)
        .then(function (result) {
            return res.json(200, result);
        })
        .catch(function (err) {
            DatabaseErrorService.log(err, res);
        })
    }
};
&nbsp;
&nbsp;</pre></td></tr>
</table></pre>
<div class='push'></div><!-- for sticky footer -->
</div><!-- /wrapper -->
<div class='footer quiet pad2 space-top1 center small'>
  Code coverage
  generated by <a href="http://istanbul-js.org/" target="_blank">istanbul</a> at Thu Feb 16 2017 18:24:11 GMT-0200 (BRST)
</div>
</div>
<script src="../../prettify.js"></script>
<script>
window.onload = function () {
        if (typeof prettyPrint === 'function') {
            prettyPrint();
        }
};
</script>
<script src="../../sorter.js"></script>
</body>
</html>
