/**
 * OppTransaction.js
 *
 * @description :: Model representation for OPP transactions
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    transactionId: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'transaction_id'
    },
    totalAmount: {
      type: Sequelize.NUMERIC,
      field: 'total_amount',
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: Sequelize.ENUM,
      values: ['deposit', 'document', 'transfer', 'establishment', 'withdraw'],
      allowNull: false 
    },
    status: {
      type: Sequelize.ENUM,
      values: ['pending', 'done', 'rejected', 'canceled'],
      allowNull: false 
    }
  },
  associations: function() {
    OppTransaction.hasMany(TransactionItem, { 
      foreignKey : { 
        name: 'transactionId', 
        field: 'transaction_id'
      } 
    });
    OppTransaction.belongsTo(OppUser, { 
      as: 'fromUser',
      foreignKey : { 
        name: 'fromUserId', 
        field: 'from_user' 
      } 
    });
    OppTransaction.belongsTo(OppUser, {
      as: 'toUser', 
      foreignKey : { 
        name: 'toUserId', 
        field: 'to_user' 
      } 
    });
  },
  options: {
    tableName: 'transactionopp',
    classMethods: {},
    instanceMethods: {
      toJSON: function () {
        var values = Object.assign({}, this.get());
        values.createdAt = values.created_at;
        values.updatedAt = values.updated_at;
        delete values.created_at;
        delete values.updated_at; 
        delete values.OppUser;
        return values;
      }
    },
    hooks: {},
    underscored: true
  }
};