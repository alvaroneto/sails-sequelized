/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized with JSON web token
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = function (req, res, next) {
    var token;

    if (req.headers && req.headers.authorization) {
        var parts = req.headers.authorization.split(' ');
        if (parts.length == 2) {
            var scheme = parts[0],
                credentials = parts[1];
            if (/^MobPago$/i.test(scheme)) {
                token = credentials;
            }
        } else {
            return res.json(401, { err: 'Format is Authorization: MobPago [token]' });
        }
    } else if (req.param('token')) {
        token = req.param('token');

        // We delete the token from param to not mess with blueprints
        delete req.query.token;
    } else {
        return res.json(401, { err: 'No Authorization header was found' });
    }
    jwToken.verify(token, function (err, oToken) {
        if (err) {
            if(err.name == 'TokenExpiredError'){
                // If session expire, token was removed from blacklist
                Jwt.destroy({where: {token: token}})
                .then(function (affectedRows) {
                })
            }
            return res.json(401, { err: 'Expired token' });
        } else {
            // Checking if the current token is on blacklist
            Jwt.findOne({where: {token: token}}).then(function (model) {
                if(model) return res.json(401, { err: 'Expired token' });
                req.session.authenticated = true;
                req.session.token = token;
                next();  
            })
            .catch(function (err) {
                if (err) return res.json(500, err);
            })
        }
    });
};