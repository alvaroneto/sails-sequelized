# MobPago v2 Backend Application

a [Sails](http://sailsjs.org) application

# Considerations about this branch

This branch refers to a Jiras task number MOB-1886 and MOB-1887. The task tell
to revise all the code associated with OppTransactions. 

## Installation

* On Ubuntu 16.04 or greater

#### NodeJS (v4.2.6) Installation

``` bash
    $ sudo apt-get install nodejs-legacy 
```

#### SailsJs Installation

``` bash
    $ sudo npm -g install sails
```

#### Postgresql (9.5) Installation

``` bash
    $ sudo apt-get update
    $ sudo apt-get install postgresql postgresql-contrib
    $ sudo -i -u postgres
    $ psql
    $ ALTER USER postgres WITH PASSWORD '<pass>';
    $ \q
    $ exit
```

#### Redis Installation
```bash
    $ sudo apt-get update
    $ sudo apt-get install build-essential
    $ sudo apt-get install tcl8.5
    $ wget http://download.redis.io/releases/redis-stable.tar.gz
    $ tar xzf redis-stable.tar.gz
    $ cd redis-stable
    $ make
    $ make test
    $ sudo make install
    $ cd utils
    $ sudo ./install_server.sh
    $ sudo service redis_6379 start
    $ redis-cli
    $ ping  // PONG
    $ sudo service redis_6379 stop
```
 
#### Tests Setup
 On project directory
 ```bash
    $ sudo npm install -g grunt-cli
    $ npm install grunt-mocha-istanbul --save-dev
    $ npm install supertest --save-dev
    $ sudo npm install should --save-dev
    $ sudo npm install mocha --save-dev
 ```
### To use the application in develop mode:

1. set environment: 
```bash 
    $ echo "export NODE_ENV=development" >> .bashrc
```
2. Run the Sails: 
```bash 
    $ sails lift
```

### To use the application in production mode:

1. set environment: 
```bash 
    $ echo "export NODE_ENV=production" >> .bashrc
```
2. Run the Sails: 
```bash 
    $ sails lift
```

### To run tests:

```bash
    $ npm test
```

### Internacionalization (Locales translate)
- Sails default Internacionalization engine documentation: [https://github.com/mashpie/i18n-node](https://github.com/mashpie/i18n-node)

## Version

Api Version: 0.3.0

## Documentation

 - Api Documentation: [https://goo.gl/lIh2Pr](https://goo.gl/lIh2Pr)
 - ERD (Entity-Relationship Diagram): [http://bit.ly/OPPMERX2](http://bit.ly/OPPMERX2)

 - Api Services 
    1. Run the project
    2. [http://localhost:1337/api/docs/](http://localhost:1337/api/docs/)

## PR (Pull Request) Acceptance Orientation

*   The commit should have little code
*   Preferably the message of Commits to tell the Task code
*   A PR should not be accepted without the acceptance of at least one reviewer
*   A PR should not be accepted without coverage to all code by test cases

## Recommendation

 Is Strongly recommended that developers uses VS Code to contribute. VS Code have certain tools that make easy test and debug this code.

## Next

1. Installments for card service
2. Credit Card Crud services

## History

1. Creation of SailsJs Application - *** Set 15, 2016 ***
2. Setup suite of development - *** Set 15, 2016 ***
3. Suite of tests set - *** Set 15, 2016 ***
4. Credit Card payment services - *** Octo 25, 2016 ***
5. Finish Credit Card payment services and BDD tests - *** Octo 28, 2016 ***