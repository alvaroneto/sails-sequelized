
var sails = require('sails');

before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift.
  this.timeout(90000);
  header = {};
  sails.lift({
    hooks: {
        "sequelize": require('sequelize'),
        // Load the hook
        "blueprints": false,
        "orm": false,
        "pubsub": false,
        // Skip grunt (unless your hook uses it)
        "grunt": false,
        "models": () => {
          return {
            connection: "test",
            migrate: "drop"
          }
        }
      }
    }, function(err, server) {
    if (err) return done(err);
    // here you can load fixtures, etc.
    done(err, sails);
  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  sails.lower(done);
});