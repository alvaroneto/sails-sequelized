var request = require('supertest')
should = require('chai').should,
  assert = require('chai').assert,
  expect = require('chai').expect,

  describe('Blueprint', function () {
    var header2 = {}
      , cookies = {}
      , login = { login: 'user0202@test.com', password: '12345678901234' };
    userId = 1
    before(function (done) {
      User.create({
        name: 'Test',
        email: 'user0202@test.com',
        password: '12345678901234'
      })
        .then(function (result) {
          userId = result.dataValues.userId;
          var login = { login: 'user0202@test.com', password: '12345678901234' };
          async.series([
            function (cb) {
              request(sails.hooks.http.app)
              .post('/v2/auth/login')
              .send(login)
              .expect(200)
              .end(function (err, res) {
                cookies = res.headers['set-cookie'];
                cookies.me = res.body.user;
                // Parse cookies to prepare string, which we will be able to use in request headers
                if (cookies) {
                  var cookiesH = cookies.map(function (cookie) {
                    return cookie.split(';')[0];
                  }).join('; ');
                }
                header2 = {
                  'Authorization': 'MobPago ' + res.body.token,
                  'Content-Type': 'application/json',
                  'Cookie': cookies
                };
                cb();
              })  
            }
          ], function () {
            done()
          })
        })
    });
    describe('Deposit', function () {
      it('make a deposit successfully', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/deposit')
          .send({
            "userId": 2,
            "amount": 500.00
          })
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.userId, 2);
            assert.strictEqual(res.body.balance, '0.00');
            assert.strictEqual(res.body.transactionId, 11);
            done();
          })
      });
      it('make a deposit without userId', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/deposit')
          .send({
            "amount": 500.00
          })
          .set(header2)
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.error.message, '"userId" is missing')
            done();
          })
      });
      it('make a deposit without amount', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/deposit')
          .send({
            userId: 2
          })
          .set(header2)
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.error.message, '"amount" is missing')
            done();
          })
      });
      it('make a deposit without OPP user', function (done) {
        OppUser.update(
          {
            blockchainHash: 'OPPTEST'
          },
          {
            where: {
              blockchainHash: 'OPP'
            }
          }
        ).then(function (instance) {
          request(sails.hooks.http.app)
            .post('/v2/opp/deposit')
            .send({
              amount:1000,
              userId: 2
            })
            .set(header2)
            .expect(404)
            .end(function (err, res) {
              assert.isNull(err);
              assert.isNotNull(res);
              assert.strictEqual(res.body.error.message, 'User was not found')
              done();
            })
        }).catch(function (err) {
          done(err)
        })
      });
      it('make a deposit without OPP user', function (done) {
        OppUser.update(
          {
            blockchainHash: 'OPP'
          },
          {
            where: {
              blockchainHash: 'OPPTEST'
            }
          }
        ).then(function (instance) {
          request(sails.hooks.http.app)
            .post('/v2/opp/deposit')
            .send({
              amount:1000,
              userId: 1
            })
            .set(header2)
            .expect(400)
            .end(function (err, res) {
              assert.isNull(err);
              assert.isNotNull(res);
              assert.strictEqual(res.body.error.message, 
              'Cannot create a transaction to the same User')
              done();
            })
        }).catch(function (err) {
          done(err)
        })
      });
      it('list the deposit transactions', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/opp/transaction/deposit/')
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.length, 4);
            done();
          })
      });
      it('list the deposit transactions with days filter', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/opp/transaction/deposit?days=7')
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.length, 4);
            done();
          })
      });
      it('list the deposit transactions with days filter paginated and limited', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/opp/transaction/deposit?days=7&page=1&limit=10')
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.length, 0);
            done();
          })
      });
      it('list the deposit transactions with incorrect filter params', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/opp/transaction/deposit?days=abc&page=1&limit=10')
          .set(header2)
          .expect(500)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.error.message, 'Server error');
            done();
          })
      });
      it('list the deposit transactions non-admin user account', function (done) {
        User.create({
          name: 'Test',
          email: 'user0202@test.com',
          password: '12345678901234'
        })
        .then(function (result) {
          userId = result.dataValues.userId;
          var login = { login: 'user0202@test.com', password: '12345678901234' };
          async.series([
            function (cb) {
              request(sails.hooks.http.app)
              .post('/v2/auth/login')
              .send(login)
              .expect(200)
              .end(function (err, res) {
                cookies = res.headers['set-cookie'];
                cookies.me = res.body.user;
                // Parse cookies to prepare string, which we will be able to use in request headers
                if (cookies) {
                  var cookiesH = cookies.map(function (cookie) {
                    return cookie.split(';')[0];
                  }).join('; ');
                }
                header2 = {
                  'Authorization': 'MobPago ' + res.body.token,
                  'Content-Type': 'application/json',
                  'Cookie': cookies
                };
                cb();
              })  
            }
          ], function () {
            done()
          })
        })
        request(sails.hooks.http.app)
          .get('/v2/opp/transaction/deposit?days=abc&page=1&limit=10')
          .set(header2)
          .expect(500)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.error.message, 'Server error');
            done();
          })
      });
      it('make a deposit without session and get unauthorized', function (done) {
        header2.Cookie = {};
        request(sails.hooks.http.app)
          .post('/v2/opp/deposit')
          .send({
            "userId": 1,
            "amount": 500.00
          })
          .set(header2)
          .expect(401)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.error.message, 'Expired Session. Please, login again.')
            done();
          })
      });
      it('list the deposit transactions without session and get unauthorized', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/opp/transaction/deposit/')
          .set(header2)
          .expect(401)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.error.message, 'Expired Session. Please, login again.')
            done();
          })
      });
    });
  });