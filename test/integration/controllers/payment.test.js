var request = require('supertest')
should = require('chai').should,
  assert = require('chai').assert,
  expect = require('chai').expect,

  describe('Blueprint', function () {
    var header2 = {}
      , cookies = {}
      , login = { login: 'user0202@test.com', password: '12345678901234' };
    userId = 1
    before(function (done) {
      User.create({
        name: 'Test',
        email: 'user0202@test.com',
        password: '12345678901234'
      })
        .then(function (result) {
          userId = result.dataValues.userId;
          var login = { login: 'user0202@test.com', password: '12345678901234' };
          async.series([
            function (cb) {
              request(sails.hooks.http.app)
              .post('/v2/auth/login')
              .send(login)
              .expect(200)
              .end(function (err, res) {
                cookies = res.headers['set-cookie'];
                cookies.me = res.body.user;
                // Parse cookies to prepare string, which we will be able to use in request headers
                if (cookies) {
                  var cookiesH = cookies.map(function (cookie) {
                    return cookie.split(';')[0];
                  }).join('; ');
                }
                header2 = {
                  'Authorization': 'MobPago ' + res.body.token,
                  'Content-Type': 'application/json',
                  'Cookie': cookies
                };
                cb();
              })  
            }
          ], function () {
            done()
          })
        })
    });
    describe('Payments', function () {
      it('pay a document without CC', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "fromUserId": 1,
            "amount":7.52,
            "installments":1,
            "message":"pay a document without CC",
            "useBalance":true,
            "paymentType":1,
            "paymentNumber": "10496711100000275007160682000100040000011124"
          })
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            done();
          })
      });
      it('pay a document without CC and insufficient funds', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "fromUserId": 1,
            "amount":54655555555,
            "installments":1,
            "message":"pay a document without CC",
            "useBalance":true,
            "paymentType":1,
            "paymentNumber": "10496711100000275007160682000100040000011124"
          })
          .set(header2)
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.error.message, 'Insufficient funds to complete the transaction');
            done();
          })
      });
      it('pay a document with a CC', function (done) {
        CreditCard.create({
          "nameOnCard": "Tim Berners Lee 00",
          "cardNumber": "5180923626672525",
          "cvv": "319",
          "validThru": "09-21",
          "creditCardOwnerId": 3
        })
        .then(function (cc) {
          request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "creditCardId": cc.creditCardId,
            "fromUserId": 3,
            "amount":777,
            "installments":5,
            "message":"pay a document with CC",
            "useBalance":false,
            "paymentType":1,
            "paymentNumber": "10496711100000275007160682000100040000011124"
          })
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            done();
          })
        })
        .catch(function (error) {
          done(error)
        })
      });
      it('pay a company with a CC', function (done) {
        CreditCard.create({
          "nameOnCard": "Tim Berners Lee 02",
          "cardNumber": "5180923626672525",
          "cvv": "319",
          "validThru": "09-21",
          "creditCardOwnerId": 3
        })
        .then(function (cc) {
          request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "fromUserId": 3,
            "toUserId": 2,
            "amount": 1000,
            "creditCardId": cc.creditCardId,
            "installments": 5,
            "pin": "1234",
            "message": "User payment using CC and balance",
            "useBalance": false,
            "paymentType": 3
          })
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.amount, 1000);
            assert.equal(res.body.transactionId, 14)
            assert.equal(res.body.fromUserId, 3)
            assert.equal(res.body.toUserId, 2);
            assert.equal(res.body.status, 'done');
            done();
          })
        })
        .catch(function (error) {
          done(error)
        })
      });
      it('pay a company with a CC insufficient funds', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "toUserId": 2,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 3
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.message, 'Insufficient funds to complete the transaction');
          done();
        })
      });
      it('pay a company without CC', function (done) {
        OppUser.update({
          balance: 1000
        }, 
        {
          where: {
            userId: 3
          }
        })
        .then(function (instance) {
          request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "fromUserId": 3,
            "toUserId": 2,
            "amount": 1000,
            "installments": 5,
            "pin": "1234",
            "message": "Company payment using CC and balance",
            "useBalance": true,
            "paymentType": 3
          })
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.amount, 1000);
            assert.equal(res.body.transactionId, 16)
            assert.equal(res.body.fromUserId, 3)
            assert.equal(res.body.toUserId, 2);
            assert.equal(res.body.status, 'done');
            done();
          })
        })
        .catch(function (error) {
          done(error);
        });
      });
      it('pay a OppUser without CC', function (done) {
        OppUser.update({
          balance: 1000
        }, 
        {
          where: {
            userId: 3
          }
        })
        .then(function (instance) {
          request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "fromUserId": 3,
            "toUserId": 2,
            "amount": 1000,
            "installments": 5,
            "pin": "1234",
            "message": "User payment using CC and balance",
            "useBalance": true,
            "paymentType": 2
          })
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.amount, 1000);
            assert.equal(res.body.transactionId, 17)
            assert.equal(res.body.fromUserId, 3)
            assert.equal(res.body.toUserId, 2);
            assert.equal(res.body.status, 'done');
            done();
          })
        })
        .catch(function (error) {
          done(error);
        });
      });
      it('pay a OppUser without CC and insufficient funds', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "fromUserId": 1,
            "toUserId": 2,
            "amount":54655555555,
            "installments":1,
            "message":"pay a document without CC",
            "useBalance":true,
            "paymentType":2
          })
          .set(header2)
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.error.message, 'Insufficient funds to complete the transaction');
            done();
          })
      });
      it('pay a OppUser with a CC', function (done) {
        CreditCard.create({
          "nameOnCard": "Tim Berners Lee 02",
          "cardNumber": "5180923626672525",
          "cvv": "319",
          "validThru": "09-21",
          "creditCardOwnerId": 3
        })
        .then(function (cc) {
          request(sails.hooks.http.app)
          .post('/v2/opp/payment')
          .send({
            "fromUserId": 3,
            "toUserId": 2,
            "amount": 1000,
            "creditCardId": cc.creditCardId,
            "installments": 5,
            "pin": "1234",
            "message": "Company payment using CC and balance",
            "useBalance": false,
            "paymentType": 2
          })
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.amount, 1000);
            assert.equal(res.body.transactionId, 19)
            assert.equal(res.body.fromUserId, 3)
            assert.equal(res.body.toUserId, 2);
            assert.equal(res.body.status, 'done');
            done();
          })
        })
        .catch(function (error) {
          done(error)
        })
      });
      it('pay with invalid payment type', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "toUserId": 2,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 6
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, 'Invalid paymentType');
          done();
        })
      });
      it('pay a document with invalid parameters: fromUserId', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "toUserId": 2,
          "amount": 1000,
          "installments": 5,
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 1
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"fromUserId" is missing');
          done();
        })
      });
      it('pay a document with invalid parameters: paymentType', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "amount": 1000,
          "installments": 5,
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 1
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"paymentNumber" is missing');
          done();
        })
      });
      it('pay a document with invalid parameters: amount', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "toUserId": 2,
          "installments": 5,
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 1,
          "paymentNumber":"10496711100000275007160682000100040000011124"
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"amount" is missing');
          done();
        })
      });
      it('pay a user with invalid parameters: fromUserId', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "toUserId": 2,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 2
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"fromUserId" is missing');
          done();
        })
      });
      it('pay a user with invalid parameters: toUserId', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 2
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"toUserId" is missing');
          done();
        })
      });
      it('pay a user with invalid parameters: amount', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "toUserId": 2,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 2
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"amount" is missing');
          done();
        })
      });
      it('pay a company with invalid parameters: fromUserId', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "toUserId": 2,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 3
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"fromUserId" is missing');
          done();
        })
      });
      it('pay a company with invalid parameters: toUserId', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 3
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"toUserId" is missing');
          done();
        })
      });
      it('pay a company with invalid parameters: amount', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "toUserId": 2,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 3
        })
        .set(header2)
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, '"amount" is missing');
          done();
        })
      });
      it('pay with invalid ID to the fromUserId', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 762392376,
          "toUserId": 2,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 3
        })
        .set(header2)
        .expect(404)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, 'OPP user not found');
          done();
        })
      });
      it('pay with invalid ID to the toUserId', function (done) {
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 1,
          "toUserId": 123676172,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 3
        })
        .set(header2)
        .expect(404)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, 'OPP user not found');
          done();
        })
      });
      it('unauthorized payment', function (done) {
        header2.Cookie = null
        request(sails.hooks.http.app)
        .post('/v2/opp/payment')
        .send({
          "fromUserId": 3,
          "toUserId": 2,
          "amount": 1000,
          "installments": 5,
          "pin": "1234",
          "message": "User payment using CC and balance",
          "useBalance": true,
          "paymentType": 3
        })
        .set(header2)
        .expect(401)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, 'Expired Session. Please, login again.');
          done();
        })
      });
    });
  });