// var request = require('supertest')
// should = require('chai').should,
//   assert = require('chai').assert,
//   expect = require('chai').expect,

// describe('Controller', function () {
//   login = { email: 'login@test.com', password: '123456' };
//   before(function (done) {
//     async.series([
//       function (callback) {
//         User.create({
//           name: 'Login',
//           email: 'login@test.com',
//           password: '123456',
//           oppId: 5
//         })
//           .then(function (results) {
            
//               OppUser.bulkCreate([
//                 {
//                   blockchain_hash: 'ajshsd7adhg8asgd786asd97',
//                   balance: 10000
//                 },
//                 {
//                   blockchain_hash: 'asidh820h372873he78q2e',
//                   balance: 10000
//                 },
//                 {
//                   blockchain_hash: 'masd0827h308e2083eqbgdw07q6wd67',
//                   balance: 10000
//                 },
//                 {
//                   blockchain_hash: 'kjdsf-sadhfd-7ha0878dhasd',
//                   balance: 10000
//                 }
//               ]).then(function (oppusers) {
//                 CreditCard.bulkCreate([
//                   {
//                     flag: 'VISA',
//                     lastDigits: '111111',
//                     nameOnCard: 'MILTON C CRUZ',
//                     ownerId: 1
//                   },
//                   {
//                     flag: 'MASTERCARD',
//                     lastDigits: '547895',
//                     nameOnCard: 'JADEN JEFF',
//                     ownerId: 1
//                   }
//                 ]).then(function (cc) {
//                   var admin = oppusers[0];
//                   admin.addCreditCard([cc[0], cc[1]]).then(function (user) {
//                   }).catch(function (err) {
//                     console.log(err)
//                   })
//                 })
//                 request(sails.hooks.http.app)
//                   .post('/v2/auth/login')
//                   .send(login)
//                   .expect(200)
//                   .end(function (err, res) {
//                     cookies = res.headers['set-cookie'];
//                     cookies.me = res.body.user;
//                     // Parse cookies to prepare string, which we will be able to use in request headers
//                     if (cookies) {
//                       var cookiesH = cookies.map(function (cookie) {
//                         return cookie.split(';')[0];
//                       }).join('; ');
//                     }
//                     header = {
//                       'Authorization': 'MobPago ' + res.body.token,
//                       'Content-Type': 'application/json',
//                       'Cookie': cookies
//                     };
//                     done();
//                   })
//               })
//           })
//       }
//     ], function (err, results) {
//       done();
//     });
//   });
//   describe('CreaditCard', function () {
//     it('should let\'s User create a new Creadit Card' , function (done) {
//       var document = {
//         userId: 2,
//         cardNumber: "4716790903965500",
//         nameOnCard: "AVERY CARROLL"
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/new/cc')
//         .set(header)
//         .send(document)
//         .expect(201)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.creditCard[0].lastDigits, 5500)
//           assert.equal(res.body.creditCard[0].flag, 471679)
//           done();
//         })
//     })
//     it('should pay a document with a valid Credit Card and without funds', function (done) {
//       var document = {
//         userId: 1,
//         code: '7absd8b123bas8dn21jkn298saujj',
//         amount: 70,
//         cardnumber: 111111
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(document)
//         .expect(200)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'APPROVED')
//           done();
//         })
//     });
//     it('should pay a document with a valid Credit Card and have funds', function (done) {
//       var data = {
//         userId: 1,
//         code: '7absd8b123bas8dn21jkn298sauj',
//         amount: 70,
//         cardnumber: 111111
//       }
//       async.parallel([
//         function (callback) {
//           OppUser.findOne({
//             where: { id: data.userId },
//             include: [
//               { all: true }
//             ]
//           }).then(function (user) {
//             // Associating card to owner
//             user.addCreditCard(1).then(function (user) {
//               callback(null, user)
//             })
//           })
//             .catch(function (err) {
//               console.log(err)
//             })
//         }
//       ], function (err, results) {
//         request(sails.hooks.http.app)
//           .post('/v2/opp/pay/document')
//           .set(header)
//           .send(data)
//           .expect(200)
//           .end(function (err, res) {
//             assert.isNull(err);
//             assert.isNotNull(res);
//             assert.equal(res.body.status, 'APPROVED')
//             done();
//           })
//       });
//     });
//     it('should pay a document with a invalid Credit Card', function (done) {
//       var document = {
//         userId: 1,
//         code: '7absd8b123bas8dn21jkn298sauj',
//         amount: 70,
//         cardnumber: 1122
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(document)
//         .expect(400)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'INVALIDCC')
//           done();
//         })
//     });
//     it('should pay a document with a invalid Credit Card', function (done) {
//       var document = {
//         userId: 1,
//         code: '7absd8b123bas8dn21jkn298sauj',
//         amount: 70,
//         cardnumber: 11111
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(document)
//         .expect(400)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'INVALIDCC')
//           done();
//         })
//     });
//   });
//   it('should pay a document with a invalid document code', function (done) {
//     var document = {
//       userId: 4,
//       code: 'asda9sbda9bsd9asd96',
//       amount: 70,
//       cardnumber: 111111
//     }
//     request(sails.hooks.http.app)
//       .post('/v2/opp/pay/document')
//       .set(header)
//       .send(document)
//       .expect(400)
//       .end(function (err, res) {
//         assert.isNull(err);
//         assert.isNotNull(res);
//         assert.equal(res.body.status, 'INVALIDDOC')
//         done();
//       })
//   });
//   it('should pay a document with a invalid amount', function (done) {
//     var document = {
//       userId: 4,
//       code: 'xyz456',
//       amount: 71,
//       cardnumber: 111111
//     }
//     request(sails.hooks.http.app)
//       .post('/v2/opp/pay/document')
//       .set(header)
//       .send(document)
//       .expect(400)
//       .end(function (err, res) {
//         assert.isNull(err);
//         assert.isNotNull(res);
//         assert.equal(res.body.status, 'INVALIDAMOUNT')
//         done();
//       })
//   })
//   it('should let user A pay a user B with credit card', function (done) {
//     var data = {
//       userId: 1,
//       receiverId: 2,
//       amount: 70,
//       cardnumber: 111111
//     }
//     async.parallel([
//       function (callback) {
//         OppUser.findOne({
//           where: { id: data.userId },
//           include: [
//             { all: true }
//           ]
//         }).then(function (user) {
//           // Associating card to owner
//           user.addCreditCard(1).then(function (user) {
//             callback(null, user)
//           })
//         })
//           .catch(function (err) {
//             console.log(err)
//           })
//       }
//     ], function (err, results) {
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/user')
//         .set(header)
//         .send(data)
//         .expect(200)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'APPROVED')
//           done();
//         })
//     });

//   })
//   it('should dont let user A pay a user B with credit card when amount is less then zero', function (done) {
//     var data = {
//       userId: 1,
//       receiverId: 2,
//       amount: -70,
//       cardnumber: 111111
//     }
//     request(sails.hooks.http.app)
//       .post('/v2/opp/pay/user')
//       .set(header)
//       .send(data)
//       .expect(400)
//       .end(function (err, res) {
//         assert.isNull(err);
//         assert.isNotNull(res);
//         assert.equal(res.body.status, 'INVALIDAMOUNT')
//         done();
//       })
//   })
//   it('shouldnt let some user make a payment with a invalid credit card', function (done) {
//     var data = {
//       userId: 1,
//       receiverId: 2,
//       amount: 70,
//       cardnumber: 1234
//     }
//     request(sails.hooks.http.app)
//       .post('/v2/opp/pay/user')
//       .set(header)
//       .send(data)
//       .expect(400)
//       .end(function (err, res) {
//         assert.isNull(err);
//         assert.isNotNull(res);
//         assert.equal(res.body.status, 'INVALIDCC')
//         done();
//       })
//   })
//   it('shouldnt let a user that does not exists pay another user', function (done) {
//     var data = {
//       userId: 9009,
//       receiverId: 2,
//       amount: 70,
//       cardnumber: 111111
//     }
//     request(sails.hooks.http.app)
//       .post('/v2/opp/pay/user')
//       .set(header)
//       .send(data)
//       .expect(500)
//       .end(function (err, res) {
//         assert.isNull(err);
//         assert.isNotNull(res);
//         assert.equal(res.body.status, 'UNAUTHORIZED')
//         done();
//       })
//   })
//   it('shouldnt pay a receiver that does not exists', function (done) {
//     var data = {
//       userId: 1,
//       receiverId: 9009,
//       amount: 70,
//       cardnumber: 111111
//     }
//     request(sails.hooks.http.app)
//       .post('/v2/opp/pay/user')
//       .set(header)
//       .send(data)
//       .expect(500)
//       .end(function (err, res) {
//         assert.isNull(err);
//         assert.isNotNull(res);
//         assert.equal(res.body.status, 'INVALIDDESTINATARY')
//         done();
//       })
//   })
//   it('should let a User A pay a User B using CC and him balance', function (done) {
//       var data = {
//         receiverId: 3,
//         userId: 13,
//         creditCardId: 6,
//         amount: 100,
//         useBalance: true
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(data)
//         .expect(200)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'APPROVED')
//           done();
//         })
//     })
//     it('should not let a User A pay a User B using CC and him balance if A does not exists', function (done) {
//       var data = {
//         receiverId: 3,
//         userId: 1331,
//         creditCardId: 6,
//         amount: 100,
//         useBalance: true
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(data)
//         .expect(400)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'UNAUTHORIZED')
//           done();
//         })
//     })
//     it('should not let a User A pay a User B using CC and him balance if B does not exists', function (done) {
//       var data = {
//         receiverId: 33333,
//         userId: 13,
//         creditCardId: 6,
//         amount: 100,
//         useBalance: true
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(data)
//         .expect(400)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'INVALIDDESTINATARY')
//           done();
//         })
//     })
//     it('should not let a User A pay a User B using CC and him balance if A equals to B', function (done) {
//       var data = {
//         receiverId: 13,
//         userId: 13,
//         creditCardId: 6,
//         amount: 100,
//         useBalance: true
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(data)
//         .expect(400)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'BADREQUEST')
//           done();
//         })
//     })
//     it('should not let User get a list of possible installments for come amount', function (done) {
//       var data = {
//         amount: 100
//       }
//       request(sails.hooks.http.app)
//         .post('/v2/opp/pay/document')
//         .set(header)
//         .send(data)
//         .expect(400)
//         .end(function (err, res) {
//           assert.isNull(err);
//           assert.isNotNull(res);
//           assert.equal(res.body.status, 'BADREQUEST')
//           done();
//         })
//     })
// });