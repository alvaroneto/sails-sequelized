var request = require('supertest')
should = require('chai').should,
  assert = require('chai').assert,
  expect = require('chai').expect;

describe('Blueprint', function () {
  var header = {}
    , cookies = {}
    , login = { login: 'user0101@test.com', password: '12345678901234' };
  
  before(function (done) {
    User.create({
      name: 'Test',
      email: 'user0101@test.com',
      password: '12345678901234'
    })
      .then(function (result) {
        request(sails.hooks.http.app)
        .post('/v2/auth/login')
        .send(login)
        .expect(200)
        .end(function (err, res) {
          cookies = res.headers['set-cookie'];
          cookies.me = res.body.user;
          // Parse cookies to prepare string, which we will be able to use in request headers
          if (cookies) {
            var cookiesH = cookies.map(function (cookie) {
              return cookie.split(';')[0];
            }).join('; ');
          }
          header = {
            'Authorization': 'MobPago ' + res.body.token,
            'Content-Type': 'application/json',
            'Cookie': cookies
          };
          done();
        })
      })
  });
  describe('Auth', function () {
    it('make login with a valid user', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/auth/login')
        .send(login)
        .expect(200)
        .end(function (err, res) {
          cookies = res.headers['set-cookie'];
          cookies.me = res.body.user;
          // Parse cookies to prepare string, which we will be able to use in request headers
          if (cookies) {
            var cookiesH = cookies.map(function (cookie) {
              return cookie.split(';')[0];
            }).join('; ');
          }
          header = {
            'Authorization': 'MobPago ' + res.body.token,
            'Content-Type': 'application/json',
            'Cookie': cookies
          };
          done();
        })
    });
    it('should make logout of the current User', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/auth/logout')
        .set(header)
        .expect(200)
        .end(function (err, res) {
          done();
        })
    })
  });
});