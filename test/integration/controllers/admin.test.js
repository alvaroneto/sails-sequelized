var request = require('supertest')
should = require('chai').should,
  assert = require('chai').assert,
  expect = require('chai').expect,

  describe('Blueprint', function () {
    var header2 = {}
      , cookies = {}
      , login = { login: 'user0202@test.com', password: '12345678901234' };
    userId = 1
    before(function (done) {
      User.create({
        name: 'Test',
        email: 'user0202@test.com',
        password: '12345678901234'
      })
        .then(function (result) {
          userId = result.dataValues.userId;
          var login = { login: 'user0202@test.com', password: '12345678901234' };
          async.series([
            function (cb) {
              request(sails.hooks.http.app)
              .post('/v2/auth/login')
              .send(login)
              .expect(200)
              .end(function (err, res) {
                cookies = res.headers['set-cookie'];
                cookies.me = res.body.user;
                // Parse cookies to prepare string, which we will be able to use in request headers
                if (cookies) {
                  var cookiesH = cookies.map(function (cookie) {
                    return cookie.split(';')[0];
                  }).join('; ');
                }
                header2 = {
                  'Authorization': 'MobPago ' + res.body.token,
                  'Content-Type': 'application/json',
                  'Cookie': cookies
                };
                cb();
              })  
            }
          ], function () {
            done()
          })
        })
    });
    describe('Admin', function () {
      it('get OPP financial balance', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/opp/admin/balance')
          .set(header2)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.balance, 20);
            done();
          })
      });
      it('get OPP financial balance when OPP user does not exists', function (done) {
        OppUser.update(
          {
            roleId: 2
          },
          {
            where: {
              userId: 1
            }
          }
        ).then(function (instance) {
          request(sails.hooks.http.app)
          .get('/v2/opp/admin/balance')
          .set(header2)
          .expect(404)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            done();
          })
        }).catch(function (err) {
          done(err)
        });
      });
      it('get OPP financial balance without valid user session', function (done) {
        header2.Cookie = {};
        request(sails.hooks.http.app)
          .get('/v2/opp/admin/balance')
          .set(header2)
          .expect(401)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            done();
          })
      });
    });
  });