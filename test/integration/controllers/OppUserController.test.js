var request = require('supertest')
should = require('chai').should,
  assert = require('chai').assert,
  expect = require('chai').expect,

  describe('Blueprint', function () {
    var header = {}
      , cookies = {}
      , login = { login: 'user0101@test.com', password: '12345678901234' };
    userId = 1
    before(function (done) {
      User.create({
        name: 'Test',
        email: 'user0101@test.com',
        password: '12345678901234'
      })
        .then(function (result) {
          userId = result.dataValues.userId;
          var login = { login: 'user0101@test.com', password: '12345678901234' };
          request(sails.hooks.http.app)
            .post('/v2/auth/login')
            .send(login)
            .expect(200)
            .end(function (err, res) {
              cookies = res.headers['set-cookie'];
              cookies.me = res.body.user;
              // Parse cookies to prepare string, which we will be able to use in request headers
              if (cookies) {
                var cookiesH = cookies.map(function (cookie) {
                  return cookie.split(';')[0];
                }).join('; ');
              }
              header = {
                'Authorization': 'MobPago ' + res.body.token,
                'Content-Type': 'application/json',
                'Cookie': cookies
              };
              done();
            })
        })
    });
    describe('User', function () {
      it('create a new OppUser account and return the result json', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/user/create')
          .set(header)
          .expect(201)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            done();
          })
      });
      it('get an empty User Transactions history', function (done) {
        var endpoint = '/v2/opp/user/transactions?data={"userId":"' + userId + '"}';
        request(sails.hooks.http.app)
          .get(endpoint)
          .set(header)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert(Array.isArray(res.body.items), 'empty arrays are arrays');
            assert.equal(res.body.hasMore, false);
            done();
          })
      });
      it('should get the OPP User details', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/opp/')
          .set(header)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res.body);
            assert.equal(res.body.userId, 1)
            done();
          })
      });
      it('should make a Deposit to a OPP User Wallet', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/deposit')
          .set(header)
          .send({
            "userId": userId,
            "amount": 500.00
          })
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res.body);
            done();
          })
      });
      it('should make Deposit to a OPP User Wallet with inválid amount', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/deposit')
          .set(header)
          .send({
            "userId": 1,
            "amount": -500.00
          })
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res.body);
            done();
          })
      });
      it('should make Deposit to a OPP User Wallet with inválid User', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/deposit')
          .set(header)
          .send({
            "userId": 119278389163298,
            "amount": 500.00
          })
          .expect(404)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res.body);
            assert.equal(res.body.error.message, 'User was not found')
            done();
          })
      });
      it('should make a Withdraw to a OPP User Wallet', function (done) {
        OppUser.create({ blockchain_hash: 'aksjd87h87dh8asd8ha8sd8ha8s', balance: 20.00 })
          .then(function (user) {
            request(sails.hooks.http.app)
              .post('/v2/opp/withdraw')
              .set(header)
              .send({
                "userId": user.userId,
                "amount": 20.00
              })
              .expect(200)
              .end(function (err, res) {
                assert.isNull(err);
                assert.isNotNull(res.body);
                assert.equal(res.body.balance, 0)
                done();
              })
          })
          .catch(function (err) {
            done(err);
          })

      });
      it('should make a Withdraw to a OPP User Wallet with inválid amount', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/withdraw')
          .set(header)
          .send({
            "userId": 1,
            "amount": -500.00
          })
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res.body);
            done();
          })
      });
      it('should make Withdraw to a OPP User Wallet with inválid User', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/opp/withdraw')
          .set(header)
          .send({
            "userId": 119278389163298,
            "amount": 500.00
          })
          .expect(404)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res.body);
            done();
          })
      });
      it('should make a Transfer operation between two wallets sucessfully', function (done) {
        OppUser.create({ blockchain_hash: 'hash0001', balance: 1000 })
          .then(function (userOne) {
            OppUser.create({ blockchain_hash: 'hash0002', balance: 0 })
              .then(function (userTwo) {
                request(sails.hooks.http.app)
                  .post('/v2/opp/payment')
                  .set(header)
                  .send({
                    "fromUserId": userOne.userId,
                    "toUserId": userTwo.userId,
                    "paymentType": 2,
                    "amount": 1000,
                    "message": "Swap 100 buk's"
                  })
                  .expect(200)
                  .end(function (err, res) {
                    assert.isNull(err);
                    assert.isNotNull(res.body);
                    assert.equal(res.body.amount, "1000");
                    assert.equal(res.body.status, "done");
                    done();
                  })
              })
          })
          .catch(function (err) {
            done(err);
          })

      });
      it('should make a Transfer operation between two wallets inválid amount', function (done) {
        OppUser.create({ blockchain_hash: 'hash0003', balance: 1000 })
          .then(function (userOne) {
            OppUser.create({ blockchain_hash: 'hash0004', balance: 0 })
              .then(function (userTwo) {
                request(sails.hooks.http.app)
                  .post('/v2/opp/payment')
                  .set(header)
                  .send({
                    "fromUserId": userOne.userId,
                    "toUserId": userTwo.userId,
                    "paymentType": 2,
                    "amount": -1000,
                    "message": "Swap 100 buk's"
                  })
                  .expect(400)
                  .end(function (err, res) {
                    assert.isNull(err);
                    assert.isNotNull(res.body.error);
                    assert.equal(res.body.error.message, '"amount" is wrong')
                    done();
                  })
              })
          })
          .catch(function (err) {
            done(err);
          })
      });
      it('should make a Transfer operation between two wallets with inválid Users', function (done) {
        OppUser.create({ blockchain_hash: 'hash0005', balance: 1000 })
          .then(function (userOne) {
            OppUser.create({ blockchain_hash: 'hash0006', balance: 0 })
              .then(function (userTwo) {
                request(sails.hooks.http.app)
                  .post('/v2/opp/payment')
                  .set(header)
                  .send({
                    "fromUserId": 23710923,
                    "toUserId": 9238793,
                    "paymentType": 2,
                    "amount": 1000,
                    "message": "Swap 100 buk's"
                  })
                  .expect(404)
                  .end(function (err, res) {
                    assert.isNull(err);
                    assert.isNotNull(res.body);
                    assert.equal(res.body.error.message, 'OPP user not found')
                    done();
                  })
              })
          })
          .catch(function (err) {
            done(err);
          })
      });
    });
    it('should create an User', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/opp/user/create')
        .set(header)
        .send({
          blockchain_hash: 'Lorem ipsum Lorem ipsum Lorem ipsum',
          balance: 90.00
        })
        .expect(201)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.balance, 90.00);
          done();
        })
    });
    it('should get the Transactions', function (done) {
      request(sails.hooks.http.app)
        .get('/v2/opp/user/transactions?data={"userId":1}')
        .set(header)
        .expect(200)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.lengthOf(res.body.items, 0);
          assert.equal(res.body.hasMore, false);
          assert.equal(res.body.count, 0);
          done();
        })
    });
    it('should get a Transaction by ID', function (done) {
      OppTransaction.create(
        {
          totalAmount: 100,
          type: 'deposit',
          status: 'pending',
          fromUserId: 1,
          toUserId: 2
        }
      )
      .then(function (sucessfully) {
        request(sails.hooks.http.app)
        .post('/v2/opp/transaction')
        .set(header)
        .send({
          transactionId: sucessfully.transactionId
        })
        .expect(200)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          done();
        })  
      }).catch(function (error) {
        done();
      })
      
    });
    it('should approve a Deposit Transaction', function (done) {
      OppTransaction.create({
        amount: 20.00,
        status: 'pending',
        type: 'deposit',
        fromUserId: 1,
        toUserId: 2
      })
        .then(function (tx) {
          OppUser.create({ blockchain_hash: 'asdlja98d9ha9dha0dshd0', balance: 0 })
            .then(function (user) {
              tx.setToUser(user.id).then(function (result) {
                request(sails.hooks.http.app)
                  .post('/v2/opp/transaction/approve')
                  .set(header)
                  .send({
                    transactionId: tx.transactionId,
                    type: 'deposit'
                  })
                  .expect(200)
                  .end(function (err, res) {
                    assert.isNull(err);
                    assert.isNotNull(res);
                    assert.equal(res.body.status, 'done')
                    done();
                  })
              })
            })
        }).catch(function (err) {
          done(err)
        })
    });
    it('should approve a Withdraw Transaction', function (done) {
      OppTransaction.create({
        amount: 20.00,
        status: 'pending',
        type: 'withdraw'
      })
        .then(function (tx) {
          OppUser.create({ blockchain_hash: 'lkaysd8a7hshd08asd07ads', balance: 20 })
            .then(function (user) {
              tx.setToUser(user.id).then(function (result) {
                request(sails.hooks.http.app)
                  .post('/v2/opp/transaction/approve')
                  .set(header)
                  .send({
                    transactionId: tx.transactionId,
                    type: 'withdraw'
                  })
                  .expect(200)
                  .end(function (err, res) {
                    assert.isNull(err);
                    assert.isNotNull(res);
                    assert.equal(res.body.status, 'done')
                    done();
                  })
              })
            })
        }).catch(function (err) {
          done(err)
        })
    });
    it('should get "Insuficient funds" trying approve a transaction', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/opp/withdraw')
        .set(header)
        .send({
          userId: 2,
          amount: 9999999999
        })
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, 'Insufficient funds to complete the transaction');
          done();
        })    
    });
    it('should approve a Transaction without transactionId, and return 400', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/opp/transaction/approve')
        .set(header)
        .send({
          transactionId: null
        })
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.equal(res.body.error.message, '"transactionId" is missing');
          done();
        })
    });
  });