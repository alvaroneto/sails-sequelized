var request = require('supertest')
should = require('chai').should,
  assert = require('chai').assert,
  expect = require('chai').expect,

  describe('Blueprint', function () {
    var header2 = {}
      , header3 = {}
      , cookies = {}
      , login = { login: 'user0202@test.com', password: '12345678901234' };
    userId = 1
    before(function (done) {
      User.create({
        name: 'Test',
        email: 'user0202@test.com',
        password: '12345678901234'
      })
        .then(function (result) {
          userId = result.dataValues.userId;
          var login = { login: 'user0202@test.com', password: '12345678901234' };
          async.series([
            function (cb) {
              request(sails.hooks.http.app)
              .post('/v2/auth/login')
              .send(login)
              .expect(200)
              .end(function (err, res) {
                cookies = res.headers['set-cookie'];
                cookies.me = res.body.user;
                // Parse cookies to prepare string, which we will be able to use in request headers
                if (cookies) {
                  var cookiesH = cookies.map(function (cookie) {
                    return cookie.split(';')[0];
                  }).join('; ');
                }
                header2 = {
                  'Authorization': 'MobPago ' + res.body.token,
                  'Content-Type': 'application/json',
                  'Cookie': cookies
                };
                cb();
              })  
            }
          ], function () {
            done()
          })
        })
    });
    describe('Auth', function () {
      it('login successfully', function (done) {
        User.create({
        name: 'Test',
        email: 'loginteste@test.com',
        password: '12345678901234'
      })
        .then(function (result) {
          async.series([
            function (cb) {
              request(sails.hooks.http.app)
              .post('/v2/auth/login')
              .send({ 
                login: 'loginteste@test.com', 
                password: '12345678901234' 
              })
              .expect(200)
              .end(function (err, res) {
                cookies = res.headers['set-cookie'];
                cookies.me = res.body.user;
                // Parse cookies to prepare string, which we will be able to use in request headers
                if (cookies) {
                  var cookiesH = cookies.map(function (cookie) {
                    return cookie.split(';')[0];
                  }).join('; ');
                }
                header3 = {
                  'Authorization': 'MobPago ' + res.body.token,
                  'Content-Type': 'application/json',
                  'Cookie': cookies
                };
                cb();
              })  
            }
          ], function () {
            done()
          })
        });
      })
      it('logoff ok', function (done) {
        request(sails.hooks.http.app)
          .get('/v2/auth/logout')
          .set(header3)
          .expect(200)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            done();
          })
      });
      it('login with wrong parameter', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/auth/login')
          .send({ 
            email: 'user0202@test.com', 
            password: '12345678901234' 
          })
          .set(header2)
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.equal(res.body.error.message, 'Invalid Login or Password');
            done();
          })
      });
      it('login with invalid credentials', function (done) {
        request(sails.hooks.http.app)
          .post('/v2/auth/login')
          .send({ 
            login: 'sjdlkasdkl@kajhsdk.com', 
            password: '12345678901234' 
          })
          .set(header2)
          .expect(403)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(typeof res.body, 'object')
            done();
          })
      });
      it('logoff with no session', function (done) {
        header2.Cookie = {};
        request(sails.hooks.http.app)
          .get('/v2/auth/logout')
          .set(header2)
          .expect(400)
          .end(function (err, res) {
            assert.isNull(err);
            assert.isNotNull(res);
            assert.strictEqual(res.body.error.message, 'token cannot be null')
            done();
          })
      });
    });
  });