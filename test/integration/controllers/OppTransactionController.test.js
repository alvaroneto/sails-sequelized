var request = require('supertest')
should = require('chai').should,
  assert = require('chai').assert,
  expect = require('chai').expect,

  describe('Transactions', function () {
    cookies = {}
    header = {}
    login = { login: 'test02@test.com', password: '123456' };
    before(function (done) {
      User.create({
        name: 'Test02',
        email: 'test02@test.com',
        password: '123456'
      })
        .then(function (results) {
          done();
        })
    });
    it('should make login with a valid User', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/auth/login')
        .send(login)
        .expect(200)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res.body);
          assert.isNotNull(res.body.token)
          cookies = res.headers['set-cookie'];
          cookies.me = res.body.user;
          // Parse cookies to prepare string, which we will be able to use in request headers
          if (cookies) {
            var cookiesH = cookies.map(function (cookie) {
              return cookie.split(';')[0];
            }).join('; ');
          }
          header = {
            'Authorization': 'MobPago ' + res.body.token,
            'Content-Type': 'application/json',
            'Cookie': cookies
          };
          done();
        })
    });
    it('should create an User', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/opp/user/create')
        .set(header)
        .send({
          blockchain_hash: 'Lorem ipsum Lorem ipsum Lorem ipsum',
          balance: 90.00
        })
        .expect(201)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.balance, 90.00);
          done();
        })
    });
    it('should get a Transaction Not Found Error', function (done) {
      request(sails.hooks.http.app)
        .get('/v2/opp/findtransaction?data={"transactionId": 1}')
        .set(header)
        .expect(404)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          assert.equal(res.body.error.message, 'Transaction was not found')
          done();
        })
    });
    it('should get a Transaction by ID', function (done) {
      OppTransaction.create({
        amount: 20.00,
        status: 'pending',
        type: 'deposit'
      })
      .then(function (tx) {
        request(sails.hooks.http.app)
        .get('/v2/opp/findtransaction?data={"transactionId": 1 }')
        .set(header)
        .expect(200)
        .end(function (err, res) {
          assert.isNull(err);
          assert.isNotNull(res);
          done();
        })
      })
      .catch(function (error) {
        done(error);
      })
    });
    it('should approve a Deposit Transaction', function (done) {
      OppTransaction.create({
        amount: 20.00,
        status: 'pending',
        type: 'deposit'
      })
        .then(function (tx) {
          OppUser.create({ blockchain_hash: 'asdlja98d9ha9dha0dshd0', balance: 0 })
            .then(function (user) {
              tx.setToUser(user.id).then(function (result) {
                request(sails.hooks.http.app)
                  .post('/v2/opp/transaction/approve')
                  .set(header)
                  .send({
                    transactionId: tx.transactionId,
                    type: 'deposit'
                  })
                  .expect(200)
                  .end(function (err, res) {
                    assert.isNull(err);
                    assert.isNotNull(res);
                    assert.equal(res.body.status, 'done')
                    done();
                  })
              })
            })
        }).catch(function (err) {
          done(err)
        })
    });
    it('should approve a Withdraw Transaction', function (done) {
      OppTransaction.create({
        amount: 20.00,
        status: 'pending',
        type: 'withdraw'
      })
        .then(function (tx) {
          OppUser.create({ blockchain_hash: 'lkaysd8a7hshd08asd07ads', balance: 20 })
            .then(function (user) {
              tx.setToUser(user.id).then(function (result) {
                request(sails.hooks.http.app)
                  .post('/v2/opp/transaction/approve')
                  .set(header)
                  .send({
                    transactionId: tx.transactionId,
                    type: 'withdraw'
                  })
                  .expect(200)
                  .end(function (err, res) {
                    assert.isNull(err);
                    assert.isNotNull(res);
                    assert.equal(res.body.status, 'done')
                    done();
                  })
              })
            })
        }).catch(function (err) {
          done(err)
        })
    });
    it('should approve a Transaction without transactionId, and return 400', function (done) {
      request(sails.hooks.http.app)
        .post('/v2/opp/transaction/approve')
        .set(header)
        .send({
          transactionId: null
        })
        .expect(400)
        .end(function (err, res) {
          assert.isNull(err);
          assert.equal(res.body.error.message, '"transactionId" is missing');
          done();
        })
    });
  });