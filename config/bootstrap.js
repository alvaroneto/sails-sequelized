/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function (cb) {
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  var oppUser = null;

  User.sequelize.sync({ force: (sails.config.models.migrate == 'drop') ? true : false })
    .then(function(){
      return User.findOrCreate({
        where: {
          name: 'OPP'
        },
        defaults: {
          name: 'OPP',
          email: 'admin@opp.com',
          password: '123456'
        }
      });
    })
    .then(function(instances){
      if(!instances) throw new Error('Bootstrap: Could not create admin@opp.com');
      sails.log.info('Bootstrap: Created admin@opp.com');
      return OppUser.findOne({
        where: { blockchainHash: 'OPP' }
      });
    })
    .then(function(instance){
      return OppUser.findOrCreate({
        where: { blockchainHash: 'OPP' },
        defaults: {
          blockchainHash: 'OPP',
          balance: 0
        }
      });
    })
    .then(function(instances){
      oppUser = instances[0];
      return OppUser.findOrCreate({
        where: { blockchainHash: 'test' },
        defaults: {
          blockchainHash: 'test',
          balance: 0
        }
      });
    })
     .then(function(instances){
    //   return CreditCard.bulkCreate([
    //             {
    //               flag: '1111',
    //               lastDigits: '111111',
    //               nameOnCard: 'MILTON C CRUZ',
    //               creditCardOwnerId: instances[0].userId
    //             },
    //             {
    //               flag: '1111',
    //               lastDigits: '111112',
    //               nameOnCard: 'JADEN JEFF',
    //               creditCardOwnerId: instances[0].userId
    //             }
    //           ])
    })
    .then(function(instances){
      //if(!instances.length) throw new Error('Bootstrap: Could not create test credit cards');
      sails.log.info('Bootstrap: Created test credit cards');
      return OppRoles.bulkCreate([{
                  name: "OPP"
                }, {
                  name: "Admin"
                }, {
                  name: "User"
                }]);
    })
    .then(function(instances){
      if(!instances) throw new Error('Bootstrap: Could not create Roles');
      sails.log.info('Bootstrap: Created Roles');
      return OppRoles.findOne({where: {name: "OPP"}});
    })
    .then(function(instance){
      oppUser.roleId = instance.roleId;
      return oppUser.save();
    })
    .then(function(instance){
     /* Important: this will create a trigger to keep track of every change made to the
     * transaction_item table. On every update to the transaction_item it will insert a 
     * new line to the transaction_item_history.
     */
    return User.sequelize.query(
       ' CREATE OR REPLACE FUNCTION transaction_item_trigger() RETURNS trigger AS $transaction_item_trigger$'
      +'    BEGIN'
      +'       INSERT INTO transaction_item_history (amount, installments, payment_number, payment_method, status, created_at, updated_at, updated_by, credit_card_id, transaction_id, transaction_item_id)'
      +'       VALUES (NEW.amount, NEW.installments, NEW.payment_number, NEW.payment_method::TEXT, NEW.status::TEXT, NEW.created_at, NEW.updated_at, NEW.updated_by, NEW.credit_card_id, NEW.transaction_id, NEW.transaction_item_id);'
      +'       RETURN NULL;'
      +'    END;'
      +' $transaction_item_trigger$ LANGUAGE plpgsql;'

      +' DROP TRIGGER IF EXISTS transaction_item_trigger on transaction_item;'

      +' CREATE TRIGGER transaction_item_trigger'
      +'  AFTER UPDATE ON transaction_item'
      +'  FOR EACH ROW EXECUTE PROCEDURE transaction_item_trigger();')
    })
    .then(function(instance){
      if(!instance) throw new Error('Bootstrap: Could not create Roles');
      sails.log.info('Bootstrap: Created Transaction Item Trigger');
      cb();
    })
    .catch(function(err){
      return cb(err);
    });
};