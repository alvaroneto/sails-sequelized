var bunyan = require('bunyan')
  , path   = require('path');

module.exports.bunyan = {
  /** If true, a child logger is injected on each request */
  injectRequestLogger: true,
 
  /** If true, log uncaughtExceptions and terminate the process */
  logUncaughtException: false,
 
  /** If given, signal to listen on for file rotation */
  rotationSignal: 'SIGUSR',
 
  logger: {
    /** Logger name */
    name: 'sails',
    /** Bunyan logging level; defaults to info */
    level: 'info',
    /** Bunyan serializers */
    serializers: bunyan.stdSerializers,
    /** Array of output streams */
    streams: [
        {
            level: 'info',
            stream: process.stdout            // log INFO and above to stdout
        },
        {
            level: 'info',
            type: 'rotating-file',
            path: './log/info.log',           // log INFO and above to stdout
            period: '1d'   // daily rotation
        },
        {
            level: 'debug',
            type: 'rotating-file',
            path: './log/debug.log',           // log INFO and above to stdout
            period: '1d'   // daily rotation
        },
        {
            level: 'warn',
            type: 'rotating-file',
            path: ('./log/warn.log'),  // log ERROR and above to a file 
            period: '1d'   // daily rotation
        },
        {
            level: 'error',
            type: 'rotating-file',
            path: ('./log/error.log'),  // log ERROR and above to a file 
            period: '1d'   // daily rotation
        }
    ]
  }
};