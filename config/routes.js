/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

// Dynamic include custom routes from api
// To include dynamic routes just create 
// a router file in folder ./api/routes
var normalizedPath = require("path").join(__dirname, "../api/routes")
  , requireDic = [];

require("fs").readdirSync(normalizedPath).forEach(function(file) {
  requireDic.push(require("../api/routes/" + file)());
});

var routes = function (dic) {
  var obj = {};
  dic.forEach(function (routes) {
    for (p in routes) {
      obj[p] = routes[p];
    }
  })
  return obj;
}

module.exports.routes = routes(requireDic);
